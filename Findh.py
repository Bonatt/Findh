import ROOT
import numpy as np
import math
#import os
#import linecache
#import sys
import random


#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(51); # 51:blue. 52:lo=k/hi=w. 53:lo=r/hi=y, 56 rev. 54:lo=b/hi=y. 55:lo=b/hi=r, pale rainbow. none:rainbow 
# Palettes, see https://root.cern.ch/doc/master/classTColor.html#C05. Or for ROOT 5.34: https://people.nscl.msu.edu/~noji/colormap
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
#ROOT.gStyle.SetTitleFont(132, 't');
#ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(1);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return np.sqrt(x)
def sin(x):
  return np.sin(x)
def cos(x):
  return np.cos(x)
# Redefine pi to something shorter
pi = np.pi

print ''









circ1Color = ROOT.kRed #Violet
circ2Color = ROOT.kGreen #Cyan
circ3Color = ROOT.kBlue #Orange
circlineStyle = ROOT.kSolid
circlineWidth = 3

#lineColor = ROOT.kGray
line12Color = ROOT.kOrange#Yellow
line23Color = ROOT.kCyan
line31Color = ROOT.kMagenta
lineStyle = ROOT.kDashed
lineWidth = 1




# See image in current dir. Let first circle be centered at (0,0). 
# An equilateral trangle (60 deg angles) may be formed from the centers of the circles R apart.
# For the circles the two other centers must be 60/2. degrees and 2*R distance apart.

# Circle 1
R = 0.5
x01 = 0.
y01 = 0.
circ1 = ROOT.TEllipse(x01,y01,R,R)
circ1.SetLineStyle(circlineStyle)
circ1.SetLineWidth(circlineWidth)
circ1.SetLineColor(circ1Color)
circ1.SetFillStyle(0)
# Circle 2
deg = 120 #30
degTorad = pi/180.
x02 = x01 + 2*R*cos(deg*degTorad)
y02 = y01 + 2*R*sin(deg*degTorad)
circ2 = ROOT.TEllipse(x02,y02,R,R)
circ2.SetLineStyle(circlineStyle)
circ2.SetLineWidth(circlineWidth)
circ2.SetLineColor(circ2Color)
circ2.SetFillStyle(0)
# Circle 3
x03 = x01 + 2*R*cos((deg-60)*degTorad) #-deg*degTorad)
y03 = y01 + 2*R*sin((deg-60)*degTorad) #-deg*degTorad)
circ3 = ROOT.TEllipse(x03,y03,R,R)
circ3.SetLineStyle(circlineStyle)
circ3.SetLineWidth(circlineWidth)
circ3.SetLineColor(circ3Color)
circ3.SetFillStyle(0)


# Center of all three circles
x0 = (x01+x02+x03)/3.
y0 = (y01+y02+y03)/3.






c = ROOT.TCanvas('c', 'c', 720, 720)
xRange1 = x0 - 2*R - R/4
yRange1 = y0 - 2*R - R/4
xRange2 = x0 + 2*R + R/4
yRange2 = y0 + 2*R + R/4
'''
c = ROOT.TCanvas('c', 'c', 720*5/4, 720) # 1366
xRange1 = x0 - 2*R - R/4
yRange1 = y0 - 2*R - R/4
xRange2 = (x0 + 2*R + R/4)*3/2 #+ (x0 + 2*R + R/4)*3/2
yRange2 = y0 + 2*R + R/4 
'''
c.Range(xRange1, yRange1, xRange2, yRange2)
circ1.Draw()
circ2.Draw()
circ3.Draw()





# Line from x01 to x02, etc, making equilateral triangle
line12 = ROOT.TLine(x01, y01, x02, y02)
line12.SetLineColor(line12Color)
line12.SetLineStyle(lineStyle)
line12.SetLineWidth(lineWidth)
line12.Draw()
# Line from x02 to x03
line23 = ROOT.TLine(x02, y02, x03, y03)
line23.SetLineColor(line23Color)
line23.SetLineStyle(lineStyle)
line23.SetLineWidth(lineWidth)
line23.Draw()
# Line from x03 to x01
line31 = ROOT.TLine(x03, y03, x01, y01)
line31.SetLineColor(line31Color)
line31.SetLineStyle(lineStyle)
line31.SetLineWidth(lineWidth)
line31.Draw()


# Line from where circ1 and circ2 touch (or midpoint of line segment 12) to center of circles
x12 = (x01+x02)/2. #x01 + R*cos(deg*degTorad)
y12 = (y01+y02)/2. #y01 + R*sin(deg*degTorad)
line012 = ROOT.TLine(x12, y12, x0, y0)
line012.SetLineColor(line12Color)
line012.SetLineStyle(lineStyle)
line012.SetLineWidth(lineWidth)
line012.Draw()
# Line from where circ3 and circ3 touch (or midpoint of line segment 23) to center of circles
x23 = (x02+x03)/2.
y23 = (y02+y03)/2.
line023 = ROOT.TLine(x23, y23, x0, y0)
line023.SetLineColor(line23Color)
line023.SetLineStyle(lineStyle)
line023.SetLineWidth(lineWidth)
line023.Draw()
# Line from where circ3 and circ1 touch (or midpoint of line segment 31) to center of circles
x31 = (x03+x01)/2.
y31 = (y03+y01)/2.
line031 = ROOT.TLine(x31, y31, x0, y0)
line031.SetLineColor(line31Color)
line031.SetLineStyle(lineStyle)
line031.SetLineWidth(lineWidth)
line031.Draw()


# Line from center of circ1 to center of circles
line010 = ROOT.TLine(x01, y01, x0, y0)
line010.SetLineColor(circ1Color)
line010.SetLineStyle(lineStyle)
line010.SetLineWidth(lineWidth)
line010.Draw()
# Line from center of circ2 to center of circles
line020 = ROOT.TLine(x02, y02, x0, y0)
line020.SetLineColor(circ2Color)
line020.SetLineStyle(lineStyle)
line020.SetLineWidth(lineWidth)
line020.Draw()
# Line from center of circ3 to center of circles
line030 = ROOT.TLine(x03, y03, x0, y0)
line030.SetLineColor(circ3Color)
line030.SetLineStyle(lineStyle)
line030.SetLineWidth(lineWidth)
line030.Draw()


# Line from where circ1 and circ2 touch to where circ3 and circ1 touch
line1231 = ROOT.TLine(x12, y12, x31, y31)
line1231.SetLineColor(circ1Color)
line1231.SetLineStyle(lineStyle)
line1231.SetLineWidth(lineWidth)
line1231.Draw()
# Line from where circ1 and circ2 touch to where circ2 and circ3 touch
line1223 = ROOT.TLine(x12, y12, x23, y23)
line1223.SetLineColor(circ2Color)
line1223.SetLineStyle(lineStyle)
line1223.SetLineWidth(lineWidth)
line1223.Draw()
# Line from where circ2 and circ3 touch to where circ3 and circ1 touch
line2331 = ROOT.TLine(x23, y23, x31, y31)
line2331.SetLineColor(circ3Color)
line2331.SetLineStyle(lineStyle)
line2331.SetLineWidth(lineWidth)
line2331.Draw()


# Line from center of circ1 to bottom of circ3
'''
line0131 = ROOT.TLine(x01, y01, x03, y03-R)
line0131.SetLineColor(line31Color)
line0131.SetLineStyle(lineStyle)
line0131.SetLineWidth(lineWidth)
line0131.Draw()
'''



# y = mx + b
# (y2-y1) = m(x2-x1) + b
# Want line parallel to line1231 =  x12, y12, x31, y31
# (y31-y12) = m*(x31-x12) + b
hm = (y31-y12)/(x31-x12)
hb = (y31-y12) - hm*(x31-x12)




# Show h lines
hx = x0 + 1.85*R #2.5*R
hy1 = y01+R
hline1 = ROOT.TLine(x01, hy1, hx, hy1)
hline1.SetLineColor(ROOT.kBlack)
hline1.SetLineStyle(circlineStyle)
hline1.SetLineWidth(lineWidth)
hline1.Draw()
hy2 = y03-R
hline2 = ROOT.TLine(x03, hy2, hx, hy2)
hline2.SetLineColor(ROOT.kBlack)
hline2.SetLineStyle(circlineStyle)
hline2.SetLineWidth(lineWidth)
hline2.Draw()
ROOT.TLatex().DrawLatex(hx,(hy1+hy2)/2-0.0125,'Find h')

# Show radius line
rline = ROOT.TLine(x01, y01, x01+R, y01)
rline.SetLineColor(ROOT.kBlack)
rline.SetLineStyle(circlineStyle)
rline.SetLineWidth(lineWidth)
rline.Draw()
ROOT.TLatex().DrawLatex((x01+R)/2, y01+R*0.05,'r = '+str(R))




c.SaveAs('Findh.png')




#print ' h is difference between center of all circles and average y-value of red circle:', abs(y0-(y12+y31)/2)
#print ' h is difference between (center of all circles) and (center of blue or green circle - R): abs(y0-(y)
print ' h is difference between (center of red circle plus R) and (center of blue/green circle minus R):', abs(hy1-hy2)
print ' Note: I believe this answer is not general, and changes based on coordinate system. What would it be in general?'
print ''




# 1-sin(60*degTorad) = 0.1339745962155614
# 1-cos(30*degTorad) = 0.13397459621556129
# A = 60; tline = ROOT.TLine(x01,y01, x01+2*R*cos(A*degTorad), y01+2*R*sin(A*degTorad)); tline.Draw()


# abs(y01-y0) = 0.57735026918962573
# abs(y01-y0)-R = 0.077350269189625731

# 1/cos(30*degTorad) = 1.1547005383792515
# 1/cos(30*degTorad)-1 = 0.15470053837925146

# sqrt((x01-x03)**2 + (y01-(y03-R))**2) = 0.61965683746373801

circ11 = ROOT.TEllipse(x01,y01, abs(y01-y0), abs(y01-y0))
circ11.SetLineStyle(circlineStyle)
circ11.SetLineWidth(circlineWidth)
#circ11.SetLineColor(circ3Color)
circ11.SetLineWidth(lineWidth)
circ11.SetFillStyle(0)
circ11.Draw()

circ12 = ROOT.TEllipse(x01,y01, sqrt((x01-x03)**2 + (y01-(y03-R))**2), sqrt((x01-x03)**2 + (y01-(y03-R))**2))
circ12.SetLineStyle(circlineStyle)
circ12.SetLineWidth(circlineWidth)
#circ12.SetLineColor(circ3Color)
circ12.SetLineWidth(lineWidth)
circ12.SetFillStyle(0)
circ12.Draw()

c.SaveAs('Findh2.png')
