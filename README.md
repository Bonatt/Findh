### I was prompted by this image:

![prompt](prompt.jpg)

And my fun visualization:

![Findh](Findh.png)

```
h is difference between (center of red circle plus R) and (center of blue/green circle minus R): 0.133974596216
Note: I believe this answer is not general, and changes based on coordinate system. What would it be in general?
```

I don't know if this answer is correct, and I also have this other plot with some concentric circles for some intersections.
Coming back to this code, I am not entirely sure what I was doing here.

![Findh2](Findh2.png)
